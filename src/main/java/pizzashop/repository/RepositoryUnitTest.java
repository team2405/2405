package pizzashop.repository;

import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import static org.mockito.Mockito.mock;

public class RepositoryUnitTest {
    PaymentRepository repository;

    Payment payment1;
    Payment payment2;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        this.repository = new PaymentRepository();

        payment1 = mock(Payment.class);
        payment2 = mock(Payment.class);

        Mockito.when(payment1.getAmount()).thenReturn(10.0);
        Mockito.when(payment2.getAmount()).thenReturn(10.0);

        Mockito.when(payment1.getType()).thenReturn(PaymentType.Card);
        Mockito.when(payment2.getType()).thenReturn(PaymentType.Cash);

        repository.add(payment1);
    }

    @org.junit.jupiter.api.Test
    void getPaymentsReturnsPayments() {
        assert(repository.getAll().size() == 1);
        assert(repository.getAll().get(0).getAmount() == 10.0);
        assert(repository.getAll().get(0).getType() ==  PaymentType.Card);
    }

    @org.junit.jupiter.api.Test
    void addPaymentModifyPayments() {
        repository.add(payment2);
        assert(repository.getAll().size() == 2);
        assert(repository.getAll().get(1).getAmount() == 10.0);
        assert(repository.getAll().get(1).getType() ==  PaymentType.Cash);
    }

}
