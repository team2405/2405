package pizzashop.service;

import org.mockito.Mock;
import org.mockito.Mockito;
import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.IPaymentRepository;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import javax.print.attribute.HashAttributeSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.mock;

public class ServiceUnitTest {
    private PaymentRepository paymentRepository;
    private MenuRepository menuRepository;
    private PizzaService pizzaService;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        paymentRepository = mock(PaymentRepository.class);
        menuRepository = mock (MenuRepository.class);
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @org.junit.jupiter.api.Test
    void getPaymentsReturnsPayments(){
        Payment p1= new Payment(1, PaymentType.Cash, 10);
        Payment p2= new Payment(2, PaymentType.Cash, 11);
        ArrayList<Payment> payments = new ArrayList<>();
        payments.add(p1);
        payments.add(p2);
        Mockito.when(paymentRepository.getAll()).thenReturn(payments);

        assert(pizzaService.getPayments() == payments);
    }

    @org.junit.jupiter.api.Test
    void addPaymentsModifyPayments(){
        Mockito.when(paymentRepository.getAll()).thenReturn(new ArrayList<>());
        assert(pizzaService.getPayments().isEmpty());
        try {
            Payment p= new Payment(1, PaymentType.Cash, 10);
            pizzaService.addPayment(1,PaymentType.Cash, 10 );
            Mockito.when(paymentRepository.getAll()).thenReturn(Arrays.asList(p));
            assert(pizzaService.getPayments().size() == 1);
        } catch (Exception e) {
            assert(false);
        }
    }
}


