package pizzashop.service;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.Collections;

class PizzaServiceTestF02 {

    private MenuRepository menuRepository;
    private PaymentRepository paymentRepository;
    private PizzaService pizzaService;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        menuRepository = new MenuRepository();
        paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void getTotalAmountListIsNull_ReturnValueIs0() throws Exception {
        double totalCard = pizzaService.getTotalAmount(PaymentType.Card);
        assert (totalCard == 0);
    }

    @org.junit.jupiter.api.Test
    void getTotalAmountListIsEmpty_ReturnValueIs0() throws Exception {
        paymentRepository.setPaymentList(Collections.emptyList());
        double totalCard = pizzaService.getTotalAmount(PaymentType.Card);
        assert (totalCard == 0);
    }

    @org.junit.jupiter.api.Test
    void getTotalAmountForCard_ListHasOnlyCashPayments_ReturnValueIs0() throws Exception {
        pizzaService.addPayment(1, PaymentType.Cash, 80);
        double totalCard = pizzaService.getTotalAmount(PaymentType.Card);
        assert (totalCard == 0);
    }

    @org.junit.jupiter.api.Test
    void getTotalAmountForCard_ListHasCardPayments_ReturnValueIsSumOfPayments() throws Exception {
        pizzaService.addPayment(1, PaymentType.Card, 80);
        pizzaService.addPayment(1, PaymentType.Card, 20);
        double totalCard = pizzaService.getTotalAmount(PaymentType.Card);
        assert (totalCard == 100);
    }
}