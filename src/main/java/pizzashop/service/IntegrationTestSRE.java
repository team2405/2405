package pizzashop.service;

import pizzashop.model.MenuDataModel;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.util.ArrayList;
import java.util.List;

public class IntegrationTestSRE {
    private MenuRepository menuRepository;
    private PaymentRepository paymentRepository;
    private PizzaService pizzaService;
    Payment payment = new Payment(1, PaymentType.Card, 2);
    ArrayList<Payment> payments = new ArrayList<>();

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        menuRepository = new MenuRepository();
        paymentRepository = new PaymentRepository();
        payments.add(payment);
        paymentRepository.setPaymentList(payments);
        pizzaService = new PizzaService(menuRepository, paymentRepository);

    }

    @org.junit.jupiter.api.Test
    void getMenuReturnsMenu(){
        List<MenuDataModel> menu = menuRepository.getMenu();
        for(int i = 0; i < menu.size(); i++){
            assert(menu.get(i).equals(pizzaService.getMenuData().get(i)));
        }
    }

    @org.junit.jupiter.api.Test
    void getPaymentsReturnsPayments(){
        List<Payment> payments = paymentRepository.getAll();
        assert(payments == pizzaService.getPayments());
    }

    @org.junit.jupiter.api.Test
    void addPaymentsModifyPayments(){
        List<Payment> payments = pizzaService.getPayments();
        assert(payments == payments);
        try {
            pizzaService.addPayment(3, PaymentType.Cash, 3);
            assert(pizzaService.getPayments().size() == 2);
        } catch (Exception e) {
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void getTotalAmountReturnsTotalAmount(){
        double amount = pizzaService.getTotalAmount(PaymentType.Card);
        assert(amount == payment.getAmount());
        double amount1 = pizzaService.getTotalAmount(PaymentType.Cash);
        assert(amount1 == 0);
    }

}
