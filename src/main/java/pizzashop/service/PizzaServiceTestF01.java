package pizzashop.service;

import org.junit.jupiter.api.Assertions;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.*;

class PizzaServiceTestF01 {

    private MenuRepository menuRepository;
    private PaymentRepository paymentRepository;
    private PizzaService pizzaService;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        menuRepository = new MenuRepository();
        paymentRepository = new PaymentRepository();
        pizzaService = new PizzaService(menuRepository, paymentRepository);
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void idMasaBVAValid1() {
        int idMasa= 1;
        try {
            pizzaService.addPayment(idMasa, PaymentType.Cash, 80 );
        } catch (Exception e) {
           assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void amountBVAValid2() {
       double validAmount = 1;
        try {
            pizzaService.addPayment(1, PaymentType.Cash, validAmount );
        } catch (Exception e) {
            assert(false);
        }
    }

    @org.junit.jupiter.api.Test
    void idMasaBVAInvalid1() {
        int invalidIdMasa = 0;
        Assertions.assertThrows(Exception.class,
                ()->{pizzaService.addPayment(invalidIdMasa, PaymentType.Cash, 50);});

    }

    @org.junit.jupiter.api.Test
    void amountBVAInvalid2() {
        double invalidAmount = 0;
        Assertions.assertThrows(Exception.class,
                ()->{pizzaService.addPayment(1, PaymentType.Cash, invalidAmount);});
    }

    @org.junit.jupiter.api.Test
    void amountECPInvalid1() {
        double invalidAmount = -50;
        Assertions.assertThrows(Exception.class,
                ()->{pizzaService.addPayment(1, PaymentType.Cash, invalidAmount);});

    }

    @org.junit.jupiter.api.Disabled
    @org.junit.jupiter.api.Test
    void idMasaECPInvalid2() {
        int invalidIdMasa = 20;
        Assertions.assertThrows(Exception.class,
                ()->{pizzaService.addPayment(invalidIdMasa, PaymentType.Cash, 80 );});
    }


    @org.junit.jupiter.api.Test
    @org.junit.jupiter.api.DisplayName("Valid amount ECP")
    void amountECPValid1() {
        double validAmount = 100;
        try{
            pizzaService.addPayment(1, PaymentType.Cash, validAmount );
        }catch(Exception ex){
            assert(false);
        }
    }
}