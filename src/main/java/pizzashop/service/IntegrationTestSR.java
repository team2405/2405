package pizzashop.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pizzashop.model.Payment;
import pizzashop.model.PaymentType;
import pizzashop.repository.MenuRepository;
import pizzashop.repository.PaymentRepository;
import static org.mockito.Mockito.mock;

public class IntegrationTestSR {
    PizzaService service;
    PaymentRepository repo;
    MenuRepository menuRepository;

    Payment payment1;
    Payment payment2;

    @BeforeEach
    void setUp() {
        repo = new PaymentRepository();
        menuRepository = new MenuRepository();
        service = new PizzaService(menuRepository, repo);

        payment1 =mock(Payment.class);
        payment2 =mock(Payment.class);

        repo.add(payment1);

        Mockito.when(payment2.getTableNumber()).thenReturn(1);

        Mockito.when(payment1.getAmount()).thenReturn(10.0);
        Mockito.when(payment2.getAmount()).thenReturn(10.0);

        Mockito.when(payment1.getType()).thenReturn(PaymentType.Card);
        Mockito.when(payment2.getType()).thenReturn(PaymentType.Cash);
    }

    @Test
    void GetAll() {
        assert(service.getPayments().size() == 1);
        assert(service.getPayments().get(0).getAmount() == 10.0);
        assert(service.getPayments().get(0).getType() ==  PaymentType.Card);
    }

    @Test
    void Add() throws Exception {
        assert(service.getPayments().size() == 1);
        service.addPayment(payment2.getTableNumber(), payment2.getType(), payment2.getAmount());
        assert(service.getPayments().get(1).getAmount() == 10.0);
        assert(service.getPayments().get(1).getType() ==  PaymentType.Cash);
    }

}
