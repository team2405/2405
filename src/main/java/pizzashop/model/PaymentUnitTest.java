package pizzashop.model;

public class PaymentUnitTest {
    Payment payment;
    int tableNumber = 1;
    PaymentType paymentType = PaymentType.Card;
    double amount = 1;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        this.payment = new Payment(this.tableNumber, this.paymentType, this.amount);
    }

    @org.junit.jupiter.api.Test
    void getTableReturnsTableNr() {
        assert (this.payment.getTableNumber() == this.tableNumber);
    }

    @org.junit.jupiter.api.Test
    void getPaymentReturnsPaymentType() {
        assert (this.payment.getType() == this.paymentType);
    }

    @org.junit.jupiter.api.Test
    void getAmountReturnsAmount() {
        assert (this.payment.getAmount() == this.amount);
    }

    @org.junit.jupiter.api.Test
    void setTableNrModifyTableNr() {
        payment.setTableNumber(2);
        assert (this.payment.getTableNumber() == 2);
    }

    @org.junit.jupiter.api.Test
    void setPaymentTypeModifyPaymentType() {
        PaymentType new_payment = PaymentType.Card;
        payment.setType(new_payment);
        assert (this.payment.getType() == new_payment);
    }

    @org.junit.jupiter.api.Test
    void setAmountModifyAmount() {
        payment.setAmount(2);
        assert (this.payment.getAmount() == 2);
    }

}
